@extends('layouts.app')

@section('content')
<style>
    #container {
        height: 600px;
        min-width: 310px;
        max-width: 800px;
        margin: 0 auto;
    }
    .highcharts-title {
        display: none;
    }
    .loading {
        margin-top: 10em;
        text-align: center;
        color: gray;
    }
</style>

<div class="container-fluid">
    <div id="container"></div>

    <div class="row charts">
        <div class="chart col-sm-12 col-lg-6">
            {!! $radar_marriage->container() !!}
        </div>
        {!! $radar_marriage->script() !!}

        <div class="chart col-sm-12 col-lg-6">
            {!! $radar_education->container() !!}
        </div>
        {!! $radar_education->script() !!}
    </div>
    <div class="row charts">
        <div class="chart col-sm-12 col-lg-12">
            {!! $line_nation->container() !!}
        </div>
        {!! $line_nation->script() !!}
    </div>
</div>

<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/ru/ru-all.js"></script>
<script>
var data = Array.from(<?= $count_city ?>);
// Create the chart
Highcharts.mapChart('container', {
    title: {
        text: 'Highmaps basic demo'
    },

    chart: {
        map: 'countries/ru/ru-all'
    },

    mapNavigation: {
        enabled: true,
        buttonOptions: {
            verticalAlign: 'bottom'
        }
    },

    colorAxis: {
        min: 0
    },

    series: [{
        data: data,
        name: 'Population',
        states: {
            hover: {
                color: '#BADA55'
            }
        },
        dataLabels: {
            enabled: false,
        }
    }]
});
</script>
@endsection
