<?php

use App\Nation;
use Illuminate\Database\Seeder;

class NationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Nation::create(['name' => 'Не указано']);
        Nation::create(['name' => 'абазины']);
        Nation::create(['name' => 'абхазы']);
        Nation::create(['name' => 'аварцы']);
        Nation::create(['name' => 'агулы']);
        Nation::create(['name' => 'адыгейцы']);
        Nation::create(['name' => 'азербайджанцы']);
        Nation::create(['name' => 'алеуты']);
        Nation::create(['name' => 'алтайцы']);
        Nation::create(['name' => 'тубалары']);
        Nation::create(['name' => 'арабы']);
        Nation::create(['name' => 'армяне']);
        Nation::create(['name' => 'ассирийцы']);
        Nation::create(['name' => 'балкарцы']);
        Nation::create(['name' => 'башкиры']);
        Nation::create(['name' => 'белорусы']);
        Nation::create(['name' => 'болгары']);
        Nation::create(['name' => 'буряты']);
        Nation::create(['name' => 'вепсы']);
        Nation::create(['name' => 'водь']);
        Nation::create(['name' => 'гагаузы']);
        Nation::create(['name' => 'горские евреи']);
        Nation::create(['name' => 'греки']);
        Nation::create(['name' => 'грузины']);
        Nation::create(['name' => 'башкиры']);
        Nation::create(['name' => 'аджарцы']);
        Nation::create(['name' => 'сваны']);
        Nation::create(['name' => 'дагестанцы']);
        Nation::create(['name' => 'даргинцы']);
        Nation::create(['name' => 'кубачинцы']);
        Nation::create(['name' => 'долганы']);
        Nation::create(['name' => 'евреи']);
        Nation::create(['name' => 'езиды']);
        Nation::create(['name' => 'ингуши']);
        Nation::create(['name' => 'ительмены']);
        Nation::create(['name' => 'кабардинцы']);
        Nation::create(['name' => 'казахи']);
        Nation::create(['name' => 'калмыки']);
        Nation::create(['name' => 'камчадалы']);
        Nation::create(['name' => 'каракалпаки']);
        Nation::create(['name' => 'карачаевцы']);
        Nation::create(['name' => 'карелы']);
        Nation::create(['name' => 'киргизы']);
        Nation::create(['name' => 'китайцы']);
        Nation::create(['name' => 'коми']);
        Nation::create(['name' => 'коми-пермяки']);
        Nation::create(['name' => 'корейцы']);
        Nation::create(['name' => 'коряки']);
        Nation::create(['name' => 'крымские татары']);
        Nation::create(['name' => 'кумандинцы']);
        Nation::create(['name' => 'кумыки']);
        Nation::create(['name' => 'курды']);
        Nation::create(['name' => 'лакцы']);
        Nation::create(['name' => 'латыши']);
        Nation::create(['name' => 'латгальцы']);
        Nation::create(['name' => 'лезгины']);
        Nation::create(['name' => 'литовцы']);
        Nation::create(['name' => 'манси']);
        Nation::create(['name' => 'марийцы']);
        Nation::create(['name' => 'молдаване']);
        Nation::create(['name' => 'монголы']);
    }
}
