<?php

use App\City;
use Illuminate\Database\Seeder;

class CitysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::create(['codename' => 'ru-ck']);
        City::create(['codename' => 'ru-ar']);
        City::create(['codename' => 'ru-nn']);
        City::create(['codename' => 'ru-yn']);
        City::create(['codename' => 'ru-ky']);
        City::create(['codename' => 'ru-sk']);
        City::create(['codename' => 'ru-kh']);
        City::create(['codename' => 'ru-sl']);
        City::create(['codename' => 'ru-ka']);
        City::create(['codename' => 'ru-kt']);
        City::create(['codename' => 'ru-2510']);
        City::create(['codename' => 'ru-rz']);
        City::create(['codename' => 'ru-sa']);
        City::create(['codename' => 'ru-ul']);
        City::create(['codename' => 'ru-om']);
        City::create(['codename' => 'ru-ns']);
        City::create(['codename' => 'ru-mm']);
        City::create(['codename' => 'ru-ln']);
        City::create(['codename' => 'ru-sp']);
        City::create(['codename' => 'ru-ki']);
        City::create(['codename' => 'ru-kc']);
        City::create(['codename' => 'ru-in']);
        City::create(['codename' => 'ru-kb']);
        City::create(['codename' => 'ru-no']);
        City::create(['codename' => 'ru-st']);
        City::create(['codename' => 'ru-sm']);
        City::create(['codename' => 'ru-ps']);
        City::create(['codename' => 'ru-tv']);
        City::create(['codename' => 'ru-vo']);
        City::create(['codename' => 'ru-iv']);
        City::create(['codename' => 'ru-ys']);
        City::create(['codename' => 'ru-kg']);
        City::create(['codename' => 'ru-br']);
        City::create(['codename' => 'ru-ks']);
        City::create(['codename' => 'ru-lp']);
        City::create(['codename' => 'ru-ms']);
        City::create(['codename' => 'ru-ol']);
        City::create(['codename' => 'ru-nz']);
        City::create(['codename' => 'ru-pz']);
        City::create(['codename' => 'ru-vl']);
        City::create(['codename' => 'ru-vr']);
        City::create(['codename' => 'ru-ko']);
        City::create(['codename' => 'ru-sv']);
        City::create(['codename' => 'ru-bk']);
        City::create(['codename' => 'ru-ud']);
        City::create(['codename' => 'ru-mr']);
        City::create(['codename' => 'ru-cv']);
        City::create(['codename' => 'ru-cl']);
        City::create(['codename' => 'ru-ob']);
        City::create(['codename' => 'ru-sr']);
        City::create(['codename' => 'ru-tt']);
        City::create(['codename' => 'ru-to']);
        City::create(['codename' => 'ru-ty']);
        City::create(['codename' => 'ru-ga']);
        City::create(['codename' => 'ru-kk']);
        City::create(['codename' => 'ru-cn']);
        City::create(['codename' => 'ru-kl']);
        City::create(['codename' => 'ru-da']);
        City::create(['codename' => 'ru-ro']);
        City::create(['codename' => 'ru-bl']);
        City::create(['codename' => 'ru-tu']);
        City::create(['codename' => 'ru-ir']);
        City::create(['codename' => 'ru-ct']);
        City::create(['codename' => 'ru-yv']);
        City::create(['codename' => 'ru-am']);
        City::create(['codename' => 'ru-tb']);
        City::create(['codename' => 'ru-tl']);
        City::create(['codename' => 'ru-ng']);
        City::create(['codename' => 'ru-vg']);
        City::create(['codename' => 'ru-kv']);
        City::create(['codename' => 'ru-me']);
        City::create(['codename' => 'ru-ke']);
        City::create(['codename' => 'ru-as']);
        City::create(['codename' => 'ru-pr']);
        City::create(['codename' => 'ru-mg']);
        City::create(['codename' => 'ru-bu']);
        City::create(['codename' => 'ru-kn']);
        City::create(['codename' => 'ru-kd']);
        City::create(['codename' => 'ru-ku']);
        City::create(['codename' => 'ru-al']);
        City::create(['codename' => 'ru-km']);
        City::create(['codename' => 'ru-pe']);
        City::create(['codename' => 'ru-ad']);
    }
}
