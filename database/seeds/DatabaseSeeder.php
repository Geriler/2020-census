<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GendersTableSeeder::class);
        $this->call(MarriagesTableSeeder::class);
        $this->call(EducationsTableSeeder::class);
        $this->call(NationsTableSeeder::class);
        $this->call(CitysTableSeeder::class);
        $this->call(InfoUsersTableSeeder::class);
    }
}
