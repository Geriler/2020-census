<?php

use App\InfoUser;
use Illuminate\Database\Seeder;

class InfoUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(InfoUser::class, 500)->create();
    }
}
