<?php

use App\Education;
use Illuminate\Database\Seeder;

class EducationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Education::create(['name' => 'Не указано']);
        Education::create(['name' => 'Высшее']);
        Education::create(['name' => 'Неполное высшее']);
        Education::create(['name' => 'Среднее']);
        Education::create(['name' => 'Среднее (полное) общее']);
        Education::create(['name' => 'Основное общее (неполное среднее)']);
        Education::create(['name' => 'Начальное образование']);
        Education::create(['name' => 'Дошкольное']);
        Education::create(['name' => 'Не имеют']);
    }
}
