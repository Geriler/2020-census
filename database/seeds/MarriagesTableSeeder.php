<?php

use App\Marriage;
use Illuminate\Database\Seeder;

class MarriagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Marriage::create(['name' => 'Не указано']);
        Marriage::create(['name' => 'Состоящие в браке']);
        Marriage::create(['name' => 'Состоящие в супружеском союзе']);
        Marriage::create(['name' => 'Никогда не состовшие в браке']);
        Marriage::create(['name' => 'Разведены']);
        Marriage::create(['name' => 'Разошедшиеся']);
        Marriage::create(['name' => 'Вдовые']);
    }
}
