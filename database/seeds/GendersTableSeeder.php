<?php

use App\Gender;
use Illuminate\Database\Seeder;

class GendersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Gender::create(['name' => 'Не указано']);
        Gender::create(['name' => 'Мужской']);
        Gender::create(['name' => 'Женский']);
    }
}
