<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\InfoUser;
use Faker\Generator as Faker;

$factory->define(InfoUser::class, function (Faker $faker) {
    return [
        'age' => rand(16, 80),
        'gender_id' => rand(1, 3),
        'education_id' => rand(1, 9),
        'marriage_id' => rand(1, 7),
        'nation_id' => rand(1, 61),
        'city_id' => rand(1, 83),
    ];
});
