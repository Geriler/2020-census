<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoUser extends Model
{
    protected $fillable = [
        'age', 'gender_id', 'education_id', 'marriage_id', 'nation',
    ];
}
