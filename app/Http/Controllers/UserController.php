<?php

namespace App\Http\Controllers;

use App\Charts\SampleChart;
use App\City;
use App\Education;
use App\Gender;
use App\InfoUser;
use App\Marriage;
use App\Nation;
use Illuminate\Support\Facades\Cache;

class UserController extends Controller
{
    public function index()
    {
        $data = Cache::remember('data', 3600, function () {
            $educations = Cache::remember('educations', 3600, function () {
                return Education::all();
            });

            $genders = Cache::remember('genders', 3600, function () {
                return Gender::all();
            });

            $marriages = Cache::remember('marriage', 3600, function () {
                return Marriage::all();
            });

            $nations = Cache::remember('nations', 3600, function () {
                return Nation::all();
            });

            $citys = Cache::remember('citys', 3600, function () {
                return City::all();
            });

            $info = Cache::remember('info', 3600, function () {
                return InfoUser::all();
            });

            $count_city = Cache::remember('count_city', 3600, function () {
                $count = [];
                foreach (Cache::get('citys') as $city) {
                    $count[] = [$city->codename, Cache::get('info')->where('city_id', $city->id)->count()];
                }
                return $count;
            });

            return [
                'educations' => $educations,
                'genders' => $genders,
                'marriages' => $marriages,
                'nations' => $nations,
                'citys' => $citys,
                'info' => $info,
                'count_city' => $count_city,
            ];
        });

        $radar_marriage = new SampleChart();
        $labels_marriage = [];
        $count_marriage = [];
        foreach ($data['marriages'] as $marriage) {
            $labels_marriage[] = $marriage->name;
            $count_marriage[] = $data['info']->where('marriage_id', $marriage->id)->count();
        }
        $radar_marriage->labels($labels_marriage);
        $radar_marriage->dataset('Marriage', 'radar', $count_marriage)
            ->color('rgba(255, 0, 0, 1)')
            ->backgroundColor('rgba(255, 0, 0, 0.2)')
            ->options([
                'pointBackgroundColor' => 'rgba(255, 0, 0, 1)',
                'pointBorderColor' => 'rgba(255, 0, 0, 1)',
            ]);
        $radar_marriage->displayAxes(false);

        $radar_education = new SampleChart();
        $labels_education = [];
        $count_education = [];
        foreach ($data['educations'] as $education) {
            $labels_education[] = $education->name;
            $count_education[] = $data['info']->where('education_id', $education->id)->count();
        }
        $radar_education->labels($labels_education);
        $radar_education->dataset('Education', 'radar', $count_education)
            ->color('rgba(0, 255, 0, 1)')
            ->backgroundColor('rgba(0, 255, 0, 0.2)')
            ->options([
                'pointBackgroundColor' => 'rgba(0, 255, 0, 1)',
                'pointBorderColor' => 'rgba(0, 255, 0, 1)',
            ]);
        $radar_education->displayAxes(false);

        $line_nation = new SampleChart();
        $labels_nation = [];
        $count_nation = [];
        foreach ($data['nations'] as $nation) {
            $labels_nation[] = $nation->name;
            $count_nation[] = $data['info']->where('nation_id', $nation->id)->count();
        }
        $line_nation->labels($labels_nation);
        $line_nation->dataset("Nation", 'line', $count_nation)
            ->color('rgba(0, 0, 255, 1)')
            ->backgroundColor('rgba(0, 0, 255, 0.2)')
            ->options([
                'pointBackgroundColor' => 'rgba(0, 0, 255, 1)',
                'pointBorderColor' => 'rgba(0, 0, 255, 1)',
            ]);

        $line_age = new SampleChart();
        $labels_age = [];
        $count_age = [];
        

        return view('users.index', [
            'educations' => $data['educations'],
            'genders' => $data['genders'],
            'marriages' => $data['marriages'],
            'nations' => $data['nations'],
            'citys' => $data['citys'],
            'info' => $data['info'],
            'count_city' => json_encode($data['count_city']),
            'radar_marriage' => $radar_marriage,
            'radar_education' => $radar_education,
            'line_nation' => $line_nation,
        ]);
    }
}
